"strict mode"

var gameboard = document.getElementById("gameboard");
var ctx = gameboard.getContext("2d");
var canvasHeight = ctx.canvas.height;
var canvasWidth = ctx.canvas.width;
var nerdGuy = new Image();
nerdGuy.src = "nerdyguy.png";
var gameover = new Image();
gameover.src = "gameover.png";
//var test = document.getElementById("test");
var look = document.getElementById("look");


    //var sheetHeight = nerdGuy.height; //this is giving 0 for some reason
    //var sheetWidth = nerdGuy.width;   //therefore I'm going to hardcode it
    var sheetHeight = 355;
    var sheetWidth = 195;
    var spriteHeight = sheetHeight / 3;
    var spriteWidth = sheetWidth / 3;
//var spriteHeight = nerdGuy.height / 3;
//var spriteWidth = nerdGuy.width / 3;
/*
var spriteHeight = nerdGuy.height;
var spriteWidth = nerdGuy.width;
*/
var keyleft = 37;
var keyright = 39;
var gameunder = false;
var currentTile = 0;
var hinders = [];
var highscore = 0;
var her = 0;

var map1 = new Image();
map1.src = "map1.png";
var img0 = new Image();
img0.src = "oldlady.png";
var img1 = new Image();
img1.src = "car.png";
var img2 = new Image();
img2.src = "oldlady1.png";
var img3 = new Image();
img3.src = "car1.png";
var imgArray = [];
imgArray[0] = img0;
imgArray[1] = img1;
imgArray[2] = img2;
imgArray[3] = img3;


var player = {
x  : 2000 + canvasWidth/2 - 36,
y  : 370,
dx : 0,
dy : 0,
gravity : 0.5,
gravitySpeed : 0,
height : 0,
width : 0,
animation: [[0], [3, 4, 5], [6, 7, 8]],
state: 0,
radius: 5

}

/*
var hitbox = {
    xPos: player.x + 32,
    yPos: player.y - (2*spriteHeight/3),
    radius: 32
}
*/
//?

//onkeydown or press?
document.onkeypress = function (event)
    {
    if ( event.keyCode == keyright ) // venstre
    { 
        player.dx = -8;
    } 
    else if(event.keyCode == 32 && player.y == 370)// Up 
    {
        player.gravitySpeed = -16;
    }
    else if ( event.keyCode == keyleft ) // Right 
    {   
        player.dx = 8;   
    }
    
}

//jump-movement
document.onkeyup = function (event)
{
    if ( event.keyCode == keyright ) // right
    { 
    player.dx += 8;
    player.state = 1;
    }
    if ( event.keyCode == keyleft ) // left 
    { 
    player.dx += -8;
    player.state = 2;
    }
}

playerAnimation();
function playerAnimation()
{
    //finne pos på spritesheet
        //variabler
    currentTile++;
    if(currentTile >= player.animation[player.state].length)
    {
        currentTile = 0;
    }
//    var srow = currentTile%player.animation[player.state].length;
 //   var scolumn = srow/player.animation[player.state].length;

    var srow = player.animation[player.state][currentTile]%3*spriteWidth;
    var scolumn = Math.floor(player.animation[player.state][currentTile]/3)*spriteHeight;

    //var blergh = player.animation[state][player.animation[state].length]
    var mapOffset = player.x - canvasWidth/2 + 36;
    var mapMod = Math.floor(mapOffset/5000);
    var drawmap = 1000-(mapOffset+canvasWidth)%5000;
    var hinderCount = 1;

    /* for bugfixes
    look.innerHTML = 
    "mapOffset " + mapOffset + " \n mapMod " + mapMod + " \n drawmap " + drawmap + "\n calculations " + Math.floor((mapOffset + canvasWidth)/5000)
    + "\n player.animation " + player.animation[player.state]
    + "\n srow " + srow + "\n scolumn " + scolumn
    + "\n spriteHeight " + spriteHeight + "\n spriteWidth " + spriteWidth
    + "\n hinderCount " + hinderCount + "\n player.x " + player.x
        + "\n highscore " + highscore + "\n hinders.length " + hinders.length + "\n hinders " + hinders
        + "\n nerdyguy.width " + nerdGuy.height;
        */
    

    //console.log(mapMod);
    //console.log(Math.floor((mapOffset + canvasWidth)/5000));
    //console.log(drawmap);
    //console.log(mapOffset);
    //tegning

    if(player.dx <0)
    {
       player.state = 2;
       for(var jalla = 0; jalla < hinders.length; jalla++)
           {
                hinders[jalla].dx = 2*hinders[jalla].start;
           }
    }
    else if(player.dx > 0)
    {
        player.state = 1;
        for(var jalla = 0; jalla < hinders.length; jalla++)
           {
                hinders[jalla].dx = 18*hinders[jalla].start;
           }
    }
    else if(player.dx == 0)
    {
        player.state = 0;
        for(var jalla = 0; jalla < hinders.length; jalla++)
           {
                hinders[jalla].dx = 10*hinders[jalla].start;
           }
    }

    //beregne spritesheet
    
    //bevegelse
    player.gravitySpeed += player.gravity;
    player.y += player.gravitySpeed + player.dy;
    
    if(player.dx < -8)
    {
        player.dx = -8;
    }
    else if (player.dx > 8)
    {
        player.dx = 8;
    }
    
    player.x += player.dx;
            
    //tell highscore      
    if (player.x > highscore)
    {   
    highscore = player.x;
    }
    //floor
    
    if(player.y >= 370)
    {
        player.y = 370;
        
    }
    if(player.y == 370)
    {
        player.gravitySpeed = 0;
    }

//lage hinder
if (player.x % 2500 == 0) // kan ta %480 hvis det blir for treigt men maa endre verdi begge steder...
{
    
            var choice = Math.floor(Math.random()*2);
            var start = Math.floor(Math.random()*2);
            var imgnum = 0;
            var type = 0;//wat?
            if (start == 0)
            {
                start = -1;
            }
            else if(start == 1)
            {
                start = 1;
            }

            if ((choice == 0) && (start < 0) )
            {
                imgnum = 0;
            }
            else if ((choice == 0) && (start > 0) )
            {
                imgnum = 2;
            }
            else if ((choice == 1) && (start < 0) )
            {
                imgnum = 1;
            }
            else if ((choice == 1) && (start > 0) )
            {
                imgnum = 3;
            }

//            hinder (choice, start);
            hinders.push(new hinder(imgnum, start)); // maa lage array
//            push.hinderObjekt = hinder(choice, start);
        
 
}

    //beregne hinder;
    
    for (var j = 0; j < hinders.length; j++)
    {
     /*   if ((hinders[j].x - player.x) < 400 && (hinders[j].x - player.x) > -400)
        {
            hinders[j].dx = 2*hinders[j].dx;
        }*/
        if (hinders[j].dx > 0 && hinders[j].dx != 10)
        {
            hinders[j].dx = 10;
        }
        else if (hinders[j].dx < 0 && hinders[j].dx !=(-10)) // kunne kanskje bare skrevet if hinders != 10? nei fordi != tar begge, men kan ta dx < 0 && != -10
        {
            hinders[j].dx = (-10);
        }

        hinders[j].x += hinders[j].dx; //player.dx*(-1) 
    } 



        //Draw map
    ctx.beginPath();
    ctx.clearRect(0, 0, canvasWidth, canvasHeight);
    ctx.drawImage(map1, mapOffset % 5000, 0, canvasWidth, canvasHeight, 0, 0, canvasWidth, canvasHeight);
    if (Math.floor((mapOffset + canvasWidth) / 5000) > mapMod) {
        ctx.drawImage(map1, 0, 0, canvasWidth, canvasHeight, drawmap, 0, canvasWidth, canvasHeight);
    }

    //draw char/sprite
    ctx.drawImage(nerdGuy, srow, scolumn, spriteWidth, spriteHeight, canvasWidth/2-36, player.y, spriteWidth, spriteHeight);

    for (var poop = 0; poop < hinders.length; poop++)
    {
        //test.innerHTML = hinders[poop].x 
        //+ "\n" + hinders[poop].dx;
        ctx.drawImage(hinders[poop].img, 0, 0, hinders[poop].img.width, hinders[poop].img.height, hinders[poop].x, hinders[poop].y, hinders[poop].img.width, hinders[poop].img.height);

        //hitbox
        
        var sx = (canvasWidth/2-36+player.radius) - (hinders[poop].x+ 20);
        var sy = (player.y+player.radius) -  (hinders[poop].y + 20);
        var distance = Math.sqrt(sx*sx + sy *sy);
        if(distance < 20 + 20)
        {
            gameunder = true;
        }
        if (gameunder == true)
        {
            ctx.drawImage(gameover, 0, 0, gameover.width, gameover.height, 0, 0, canvasWidth, canvasHeight);
        }

}


    //playerAnimationdraw
//    ctx.drawImage(nerdGuy, player.animation[state][], 0, 0, 0, 470, player.y, 0, 0);
       

//  bare for å huske hva som er hva i drawImage();  ctx.drawImage(element, offsetx, offsety, width, height, canvasoffsetx, canvasoffsety, canvasimageWidth, canvasimageHeight)
    

requestAnimationFrame(playerAnimation);
}

    //hinder
    function hinder(imgnum, start)     //(choice, start) type?
    {
            
        //konstruksjonsrutine
                //this.ladyspeed = 2*start;
//                this.startPosition = player.x + (canvasWidth * start);
                this.x = canvasWidth/2-36 - (canvasWidth * start);                
                this.y = 370;
                this.dx = 10*start;
                this.start = start;
                this.img = imgArray[imgnum];
                // hvilket bilde
                //var....
    }
  
/* maa vel vaere i gameloopen?
                if(xlady <= player.x + (100*start))
                {
                    ladyspeed *= 10;
                }
            */


         /*    if (choice == 0)
        {
        }
        else if ( choice == 1)
        {
            this.car = function ()
            {

            }
        }
        else if (choice == 2)
        {

        }
        else if (choice == 3)
        {

        }
        else if (choice == 4)
        {

        }
        */
        //datafelt
        //this.datafeltnavn = verdi



        //metoder
        //this.metodenavn = function(parametre) {...}

